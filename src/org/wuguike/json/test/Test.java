package org.wuguike.json.test;

import org.wuguike.json.sax.InvalidJSON;
import org.wuguike.json.sax.JObject;
import org.wuguike.json.sax.JSAXHandler;
import org.wuguike.json.sax.JSAXParser;

public class Test implements JSAXHandler {

	public static void main(String[] args) throws InvalidJSON {
		JSAXParser parser = new JSAXParser(new Test());
		String json = "{\"a\":5, \"b\":[1,3,4,null,{\"c\\n\\r\\b\\f\\u0020\": 2.34}, false, true]}";
		for (int i = 0; i < json.length(); i++) {
			parser.feed(json.charAt(i));
		}
		parser.feed('\0');
	}

	@Override
	public void onMapStart() throws InvalidJSON {
		System.out.println("start map");
	}

	@Override
	public void onArrayStart() throws InvalidJSON {
		System.out.println("start array");
	}

	@Override
	public void onMapEnd() throws InvalidJSON {
		System.out.println("end map");
	}

	@Override
	public void onArrayEnd() throws InvalidJSON {
		System.out.println("end array");
	}

	@Override
	public void onJObject(JObject o) throws InvalidJSON {
		System.out.println("object : " + o.toJson(0));
	}

}
