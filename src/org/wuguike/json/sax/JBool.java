package org.wuguike.json.sax;

public class JBool implements JObject {
	private static final JBool _true = new JBool(true);
	private static final JBool _false = new JBool(false);

	public static JBool build(boolean b) {
		if (b) {
			return _true;
		} else {
			return _false;
		}
	}

	private final boolean inner_boolean;
	private final String display;

	private JBool(boolean b) {
		inner_boolean = b;
		display = inner_boolean ? "true" : "false";
	}

	public boolean getBool() {
		return inner_boolean;
	}

	@Override
	public String toJson(int indent) {
		return display;
	}

	@Override
	public String toJson(int deep, int indent) {
		return display;
	}

	@Override
	public boolean equals(JObject o) {
		return this == o;
	}

}
