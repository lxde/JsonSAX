package org.wuguike.json.sax;

public interface JSAXHandler {
	public void onMapStart() throws InvalidJSON;

	public void onArrayStart() throws InvalidJSON;

	public void onMapEnd() throws InvalidJSON;

	public void onArrayEnd() throws InvalidJSON;

	// JNumber, JNull, JBool, JString
	public void onJObject(JObject o) throws InvalidJSON;
}
