package org.wuguike.json.sax;

public class JNumber implements JObject {
	private final double inner_number;

	public JNumber(double n) {
		inner_number = n;
	}

	public double getNumber() {
		return inner_number;
	}

	@Override
	public String toJson(int indent) {
		return toJson(0, 0);
	}

	@Override
	public String toJson(int deep, int indent) {
		return String.format("%g", inner_number);
	}

	@Override
	public boolean equals(JObject o) {
		if (o.getClass() == JNumber.class) {
			return inner_number == ((JNumber) o).inner_number;
		} else {
			return false;
		}
	}
}
