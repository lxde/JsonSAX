package org.wuguike.json.sax;

public class JNull implements JObject {
	private static final JNull _null = new JNull();

	public static JNull build() {
		return _null;
	}

	private JNull() {

	}

	@Override
	public String toJson(int indent) {
		return "null";
	}

	@Override
	public String toJson(int deep, int indent) {
		return "null";
	}

	@Override
	public boolean equals(JObject o) {
		return this == o;
	}

}
