package org.wuguike.json.sax;

public interface JObject {
	public String toJson(int indent);

	String toJson(int deep, int indent);

	boolean equals(JObject o);
}
